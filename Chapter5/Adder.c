#include <stdio.h>

int addTwoNumbers(int,int); //function prototype
void printReportHeader();
void printLuckyNumber();

int globalLuckyNumber; //Declare global variable

main()
{
  int output;
  int number1;
  int number2;

  printf("What numbers would you like to add with the function?\n");
  scanf("%d",&number1);
  scanf("%d",&number2);

  output = addTwoNumbers(number1,number2);

  printf("The result is %d.\n",output);

  printReportHeader();

  printf("What is your lucky number?\n");
  scanf("%d",&globalLuckyNumber); //Lucky number defined here

  printLuckyNumber();
}
int addTwoNumbers(int number1, int number2)
{
  return number1 + number2;
}

void printReportHeader()
{
  printf("\nThis just prints to show how you can have no input or output.\n");
}

void printLuckyNumber()
{
  printf("I won't tell anyone that your lucky number is %d.\n",globalLuckyNumber);
}
