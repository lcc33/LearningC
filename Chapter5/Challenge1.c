#include <stdio.h>

int findRemainder();
void determineLarger();

int number1;
int number2;
int largerNumber;
int smallerNumber;

main()
{
  int remainder;

  printf("Hello! Today we will play with numbers. Tell me two numbers.\nNumber 1:  ");
  scanf("%d",&number1);
  printf("Number 2:  ");
  scanf("%d",&number2);
  determineLarger();
  printf("The larger number is %d.\n",largerNumber);
  remainder = findRemainder();
  printf("\nThe remainder is %d.\n",remainder);
}

void determineLarger()
{
  if (number1 > number2){
    largerNumber = number1;
    smallerNumber = number2;
  }
  if (number2 > number1){
    largerNumber = number2;
    smallerNumber = number1;
  }
  else
    printf("Neither one of those is larger than the other.\n");
}

int findRemainder()
  {
    return largerNumber % smallerNumber;
  }
