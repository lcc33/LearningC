Looping structures
y++ adds 1 to y after it is used.
++y adds 1 to y before it is used.

x-- subtracts 1 after it is used.
--x adds 1 before it is used.

+= adds itself plus the new value
    x += y // x = x + y
also -=
    x -= y // x = x - y;

While Looping
braces needed if multiple things it does

Do while loop
      do {
        stuff
      } while (condition);
 - it will execute once, then check condition. If condition isn't met, it won't execute again.

 For Loop
    for (x=startnumber; x > endNumber; x--) \\x going by one from start Number down to 1 above end Number

break; immediately ends a loop and starts program at next line after the end of the loop
continue; stops loop in iteration, skips the rest of the stop, and then starts next iteration
