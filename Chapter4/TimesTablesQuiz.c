#include <stdio.h>
#include <time.h>
#include <stdlib.h>

main() {

  int nQuestions;
  int nCorrect = 0;
  int a;
  int b;
  int answer;
  int totalAnswered = 0;

  srand(time(0));

  printf("Welcome to your quiz!\nHow many questions would you like to answer correctly? ");
  scanf("%d",&nQuestions);
  printf("\nGreat! Let's begin. Answer the following:\n");
  while (nCorrect < nQuestions){
    a = (rand() % 10) + 1;
    b = (rand() % 10) + 1;
    printf("\n%d)\t%dx%d = ",nCorrect+1,a,b);
    scanf("%d",&answer);
    totalAnswered++;
    if (answer == a*b) {
      nCorrect++;
      printf("Great job!");
    }
    else
      printf("Wrong...Try again.");


  }
  float score = nQuestions;
  score /= totalAnswered;
  printf("All done! Your overall score is %.0f%%.\n",score*100);

}
