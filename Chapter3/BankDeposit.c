#include <stdio.h>
#include <stdlib.h>
#include <time.h>

main()
{
  int bal = 1;
  float add = 0;
  float subtract = 0;
  char choice;
  int check = 0;
  srand(time(0));

  bal = (rand() % 10000) + 1;

  printf("\nHello Customer! Your current balance is $%d. What would you like to do today?\n",bal);

  while (check < 1) {
    printf("\na\tWithdraw\n");
    printf("\nb\tDeposit\n");
    scanf("%c",&choice);
    printf("\n Thank you. How much would you like to");
    switch(choice){
      case 'a': case 'A':
        printf(" withdraw?\n");
        scanf("%f",&subtract);
        check = 1;
        break;
      case 'b': case 'B':
        printf(" deposit?\n");
        scanf("%f",&add);
        check = 1;
        break;
      default:
        printf("\nInvalid response. Please choose again:\n");
        break;
    }
  }


  bal = bal + add - subtract;

  if (bal < 0)
    printf("I am sorry. You have overdrafted. You will receive a fine shortly.\n");
  else
    printf("\nThank you customer. Your new balance is $%d\n",bal);
  }
