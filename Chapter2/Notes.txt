%% Integers
- singed and unsigned
int x; % Declares and integer.
int x, y, z; % Three different integers.

%% Floats
- signed and unsigned
float operand1; % Declares a float named operand1

%% Characters
- letters and keys like escape or delete
char FirstInitial
- only stores one character. For more than one, use a character array.

- always declare these first just in case they are already used or garbage.
- set equal with an equals sign.

      char FirstInitial;
      int x;

      FirstInitial = 'L';
      x = 0;
- also can give value when declaring it
      int x=0;

%% Constants
- cannot lose value
const int x=20;

%% Printing
- printf in stdio.h
- Use conversion specifiers to print stuff outside of the ""
      %s - Take the next argument and print it as a string
      %f - Take the next argument and print it as a float
      %d - Take the next argument and print it as an int
      %c - Take the next argument and print it as a character
    - put this  inside the "" of a printf and order the things you want to replace them after.
    "%.1f" % One decimal place float


%% Scanning
- scanf("%d", &input); in stdio.h
- user can input a value and it becomes the next value.
- Use the same % as above.
- \n not necessary since user has to press enter afterwards.
- & allows you to access the correct variable.

%% Arthimatic
- typical math signs
- included in stdio.h
- follows standard order of operations
