#include <stdio.h>
#include <string.h>
void AddingArrays(int [],int [],int []);

int main(){
  int array1[4] = {1, 2, 3, 4};
  int array2[4] = {3, 4, 5, 6};
  int output[4];

  AddingArrays(array1, array2, output);

  int ii;
  for (ii=0;ii<4;ii++){
    printf("numbers: %d\n",output[ii]);
  }
}

void AddingArrays(int array1[],int array2[],int output[]){
  int ii;
  for (ii=0;ii<4;ii++){
    output[ii] = array1[ii] + array2[ii];
  }
}
