#include <stdio.h>
#include <string.h>
#include <ctype.h>

void convertToUpper(char *);
void convertToLower(char *);
void switchCase(char *);
void wackyCase(char *);

int main(){
  char *str1 = "Hello";//referencing string through pointers.
  char str2[] = "Long John";

  printf("My two strings are %s and %s\n",str1,str2);
  printf("They are %d and %d characters long.\n",strlen(str1),strlen(str2));

  convertToUpper(str2);

  char lowerStr[10];
  strcpy(lowerStr,str2);
  convertToLower(lowerStr);
  printf("I can whisper a string like so %s\n", lowerStr);

  printf("I can also switch the case:");
  char switchC[10];
  strcpy(switchC,str2);
  switchCase(switchC);

  printf("I can make my text wacky: ");
  wackyCase(str2);
  printf("And again!: ");
  switchCase(str2);
}

void convertToUpper(char *string){
  int ii = 0;
  char upperString[10];
  while (string[ii] != '\0'){
    upperString[ii] = toupper(string[ii]);
    ii++;
  }
  printf("I can yell a string like so %s\n",upperString);

}

void convertToLower(char *string){
  int ii = 0;
  char lowerString[10];
  while (string[ii] != '\0'){
    string[ii] = tolower(string[ii]);
    ii++;
  }
}

void switchCase(char *string){
  int ii = 0;
  while (string[ii] != '\0'){
    if (string[ii]>64 && string[ii]<91){
      string[ii] = string[ii] + 32;
    }
    else if (string[ii]>96 && string[ii]<123){
      string[ii] = toupper(string[ii]);
    }
    ii++;
  }
  printf("%s\n",string);
}

void wackyCase(char *string){
  int ii = 0;
  while (string[ii] != '\0'){
    if (ii%2 == 1){
      string[ii] = tolower(string[ii]);
    }
    else{
      string[ii] = toupper(string[ii]);
    }
    ii++;
  }
  printf("%s\n",string);
}
