#include <stdio.h>
#include <string.h>

void convertToUpper(char *);

int main(){

  char name[50];

  printf("Hello! What is your name?\t");
  scanf("%s",name);
  printf("Your name is %d letters long.\n",strlen(name));

  convertToUpper(name);
  printf("Uppercase: %s\n",name);

}

void convertToUpper(char *string){
  int ii = 0;
  while (string[ii] != '\0'){
    string[ii] = toupper(string[ii]);
    ii++;
  }

}
