#include <stdio.h>
#include <string.h>

int main(){
  char phrase[] = "When the going gets tough, the tough stay put!";
  char *snips[3] = {'\0'};
  int ii;

  snips[0] = "When";
  snips[1] = "tou";
  snips[2] = "ay put!";

  for (ii=0;ii<3;ii++){
    if (strstr(phrase,snips[ii]) != NULL){
      printf("Phrase found\n");
    }
    printf("%d\n",strlen(snips[ii]));
  }
}
