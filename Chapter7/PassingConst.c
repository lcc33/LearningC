#include <stdio.h>

void printArray(const int []);
main(){
  int numbers[3] = {4, 5, 6};

  printArray(numbers);
}

void printArray(const int num[]){ // const is there to be read only
  int ii;
  printf("\nI saved the following numbers:");
  for (ii = 0; ii<3; ii++)
    printf("%d",num[ii]);
  printf("\n");
}
