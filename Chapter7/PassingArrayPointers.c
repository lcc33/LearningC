#include <stdio.h>

int countLength(char []);

main(){
  char name[20] = {'\0'};

  printf("Enter your name:");
  scanf("%s",name);
  printf("The length of your name is %d characters.\n",countLength(name));
}

int countLength(char name[]){
  int ii=0;
  while (name[ii] != '\0')
    ii++;
  return ii;
}
