#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void TossDice(int []);
void PrintRoll(int []);

int main() {
  int rolls[6];
  srand(time(0));
  TossDice(rolls);
  PrintRoll(rolls);
}

void TossDice(int holder[]){
  int ii;
  for(ii=0; ii<6; ii++){
    holder[ii] = (rand() % 6) + 1;
  }
}

void PrintRoll(int holder[]){
  int ii;
  for(ii=0; ii<6; ii++){
    printf("Your number is %d\n",holder[ii]);
  }
}
