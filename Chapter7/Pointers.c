#include <stdio.h>
main (){

  int x=1;
  int *ptrX=&x;
  int iArray[5] = {1, 2, 3, 4, 5};
  int *pntArray = iArray;

  printf("The value of x is %d. I reference x by the pointer ptrX, %d\n",x,*ptrX);
  *ptrX = 5;
  printf("I changed the value of x to %d. I will reference it with pointer, %d\n",x,*ptrX);

  printf("The value of the array pointer is %p\n",pntArray);
  printf("The address of the array is also %p\n",&iArray[0]);

  printf("This points to %d\n",*pntArray);
  printf("The value of this in the array is %d\n", iArray[0]);
}
