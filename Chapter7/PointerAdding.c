#include <stdio.h>
void addWithReferenc(int *); // input a pointer
void addWithPointer(int *);

main(){
  int X;
  int Y;
  int *pntY = &Y;

  printf("Enter a value:");
  scanf("%d",&X);
  addWithReference(&X); // Use the pointer of value to give to the function

  printf("Enter another value:");
  scanf("%d",&Y);
  addWithPointer(pntY); //input the pointer
  printf("The value is now %d\n",Y);
}

void addWithReference(int *pnt){ // the Value associated with the pointer is the input.
  *pnt += 5; // Adds 5 to the value of whatever is at that pointer
  printf("The value is now %d\n",*pnt);
}

void addWithPointer(int *pnt){ //input is a pointer. 
  *pnt += 5; // it changes the value at the location of the pointer
}
