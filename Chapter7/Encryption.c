#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void getString(char [],int);
void encryptMessage(char [],int);
void decryptMessage(char [],int);
int generateKey(void);

int main(){

  int size=20;
  int choice=0;
  int shifter = 2;
  char phrase[20] = {'\0'};

  system("clear");
  srand(time(0));

  while(choice != 4){
    printf("What would you like to do today?\n");
    printf("1)\t Encrypt message\n");
    printf("2)\t Decrypt message\n");
    printf("3)\t Generate key\n");
    printf("4)\t Quit program\n");
    scanf("%d",&choice);

    switch(choice){ // Only got switch function to work with characters.
      case 1:
        printf("Enter your phrase:");
        getString(phrase,size);
        encryptMessage(phrase,shifter);
        printf("New Phrase: %s\n",phrase);
        break;

      case 2:
        printf("Enter your encrypted phrase:");
        scanf("%s",phrase);
        decryptMessage(phrase,shifter);
        printf("New Phrase: %s\n",phrase);
        break;

      case 3:
        shifter = generateKey();
        printf("New shifter: %d\n",shifter);
        break;

      case 4:
        break;

      default:
        printf("Invalid category\n");

    }
  }
}

void encryptMessage(char word[], int number){
  int ii=0;
  while (word[ii] != '\0'){
    word[ii] += number;
    ii++;
  }
}

void decryptMessage(char word[], int number){
  int ii=0;
  while (word[ii] != '\0'){
    word[ii] -= number;
    ii++;
  }
}

int generateKey(void){
  return (rand() % 10) + 1;
}

void getString(char word[], int size) {
  char singleLetter;
  int ii = 0;
  while (ii < size) {
    scanf("%c",&singleLetter);
    if ((singleLetter == '\n') && (ii > 0))
      break;
    else{
      word[ii] = singleLetter;
      ii++;
    }

  }
}
