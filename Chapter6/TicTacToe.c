#include <stdio.h>

void DisplayBoard();
void InitializeBoard();
int DetermineWin();
int board[3][3];

main(){
  int column;
  int row;
  int win = 0;
  int player = 2;

  InitializeBoard();
  DisplayBoard();

  while (win != 1){
    if (player == 1)
      player = 2;
    else
      player = 1;
    printf("Player %d turn. Pick a column:",player);
    scanf("%d",&column);
    printf("Player %d turn. Pick a row:",player);
    scanf("%d",&row);
    if (board[row][column] == 0)
      board[row][column] = player;
    else
      printf("Sorry, spot is taken. Lose turn.\n");
    DisplayBoard();
    win = DetermineWin();
  }
  printf("Player %d wins!\n",player);
}

int DetermineWin(){
  int ii;
  int tempWin=0;

  for (ii=0; ii<2; ii++){
    if (board[ii][0] == board[ii][1] && board[ii][0] == board[ii][2] && board[ii][0]>0){
      tempWin = 1;
    }
    if (board[0][ii] == board[1][ii] && board[0][ii] == board[2][ii] && board[0][ii]>0){
      tempWin = 1;
    }
    }
  if (board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[0][0]>0)
    tempWin = 1;
  if (board[0][2] == board[1][1] && board[0][2] == board[2][0] && board[0][2] > 0)
    tempWin = 1;
  return tempWin;
}

void InitializeBoard(){
  int ii;
  int mm;
  for (ii = 0; ii < 3;ii++){
    for (mm = 0; mm < 3; mm++){
      board[ii][mm] = 0;
    }
  }
}

void DisplayBoard(){
  int ii;
  int boardCount = 0;
  for (ii = 0; ii < 3; ii++){
    printf("   %d   |   %d   |   %d   \n",board[ii][0],board[ii][1],board[ii][2]);
    printf("-------|-------|-------\n");
  }

}
